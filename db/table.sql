-- we don't know how to generate database vtk (class Database) :(
create sequence prog1.client_id_seq;

create sequence prog1.doctor_id_seq;

create sequence prog1.visit_id_seq;

create sequence prog1.list_specialist_id_seq;

create sequence prog1.account_id_seq;

create table prog1.client
(
  id        smallserial not null
    constraint id_client
    primary key,
  name      varchar(16) not null,
  familya   varchar(32) not null,
  otchestvo varchar(16) not null,
  phone     bigint      not null,
  email     varchar(96),
  address   varchar(64)
);

create table prog1.list_specialist
(
  name varchar(32) not null,
  id   smallserial not null
    constraint id_list_specialist
    primary key
);

create table prog1.doctor
(
  id            smallserial not null
    constraint id_doctor
    primary key,
  name          varchar(16) not null,
  familya       varchar(32) not null,
  otchestvo     varchar(16) not null,
  phone         bigint,
  email         varchar(96),
  address       varchar(64),
  speciality_id smallint
    constraint doctor_list_specialist_id_fk
    references list_specialist
);

create table prog1.visit
(
  id         serial    not null
    constraint id_visit
    primary key,
  doctor_id  smallint  not null
    constraint visit_doctor_id_fk
    references doctor,
  client_id  smallint  not null
    constraint visit_client_id_fk
    references client,
  visit_date timestamp not null,
  anamnesis  text,
  cost       integer,
  fullfiled  boolean
);

create unique index list_specialist_name_uindex
  on prog1.list_specialist (name);

create table prog1.account
(
  id       smallserial not null
    constraint id_account
    primary key,
  login    varchar(8),
  password varchar(8)
);

create view prog1.view_visits as
  SELECT
    v.id,
    concat_ws(' ' :: text, d.name, d.familya) AS doc,
    concat_ws(' ' :: text, c.name, c.familya) AS client,
    v.visit_date,
    v.anamnesis,
    v.cost,
    v.fullfiled
  FROM ((prog1.visit v
    LEFT JOIN prog1.doctor d ON ((v.doctor_id = d.id)))
    LEFT JOIN prog1.client c ON ((v.client_id = c.id)));


