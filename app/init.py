#!/usr/bin/python3.5
import os
import sys

from PyQt5 import QtWidgets as Wg

import oDb
import gui
from termcolor import colored, cprint

app = Wg.QApplication(sys.argv)

if not oDb.connect_to_db():
  cprint('Error connect to database', 'red', 'on_grey', ['bold'])
  sys.exit()

auth = gui.WidMain()

sys.exit(app.exec_())
