#QDialogButtonBox -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'client.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OClient(object):
    def setupUi(self, OClient):
        OClient.setObjectName("OClient")
        OClient.setWindowModality(QtCore.Qt.WindowModal)
        OClient.resize(320, 261)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(OClient.sizePolicy().hasHeightForWidth())
        OClient.setSizePolicy(sizePolicy)
        OClient.setFrameShape(QtWidgets.QFrame.NoFrame)
        OClient.setFrameShadow(QtWidgets.QFrame.Plain)
        OClient.setLineWidth(0)
        self.verticalLayout = QtWidgets.QVBoxLayout(OClient)
        self.verticalLayout.setContentsMargins(16, 3, 16, 3)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.ename = QtWidgets.QLineEdit(OClient)
        self.ename.setObjectName("ename")
        self.verticalLayout.addWidget(self.ename)
        self.efamil = QtWidgets.QLineEdit(OClient)
        self.efamil.setObjectName("efamil")
        self.verticalLayout.addWidget(self.efamil)
        self.efather = QtWidgets.QLineEdit(OClient)
        self.efather.setObjectName("efather")
        self.verticalLayout.addWidget(self.efather)
        self.ephone = QtWidgets.QLineEdit(OClient)
        self.ephone.setObjectName("ephone")
        self.verticalLayout.addWidget(self.ephone)
        self.eemail = QtWidgets.QLineEdit(OClient)
        self.eemail.setObjectName("eemail")
        self.verticalLayout.addWidget(self.eemail)
        self.eaddress = QtWidgets.QLineEdit(OClient)
        self.eaddress.setObjectName("eaddress")
        self.verticalLayout.addWidget(self.eaddress)
        self.btns = QtWidgets.QDialogButtonBox(OClient)
        self.btns.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.btns.setCenterButtons(True)
        self.btns.setObjectName("btns")
        self.verticalLayout.addWidget(self.btns)

        self.retranslateUi(OClient)
        QtCore.QMetaObject.connectSlotsByName(OClient)

    def retranslateUi(self, OClient):
        _translate = QtCore.QCoreApplication.translate
        OClient.setWindowTitle(_translate("OClient", "Клиент"))
        self.ename.setPlaceholderText(_translate("OClient", "Имя"))
        self.efamil.setPlaceholderText(_translate("OClient", "Фамилия"))
        self.efather.setPlaceholderText(_translate("OClient", "Отчество"))
        self.ephone.setPlaceholderText(_translate("OClient", "Телефон"))
        self.eemail.setPlaceholderText(_translate("OClient", "E-mail"))
        self.eaddress.setPlaceholderText(_translate("OClient", "Адрес"))

