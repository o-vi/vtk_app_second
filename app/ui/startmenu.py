# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'startmenu.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_StartMenu(object):
    def setupUi(self, StartMenu):
        StartMenu.setObjectName("StartMenu")
        StartMenu.resize(685, 480)
        self.horizontalLayout = QtWidgets.QHBoxLayout(StartMenu)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.boxMenu = QtWidgets.QFrame(StartMenu)
        self.boxMenu.setMinimumSize(QtCore.QSize(354, 0))
        self.boxMenu.setObjectName("boxMenu")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.boxMenu)
        self.verticalLayout.setObjectName("verticalLayout")
        self.bNewVisit = QtWidgets.QCommandLinkButton(self.boxMenu)
        self.bNewVisit.setObjectName("bNewVisit")
        self.verticalLayout.addWidget(self.bNewVisit)
        self.bViewVisit = QtWidgets.QCommandLinkButton(self.boxMenu)
        self.bViewVisit.setDescription("")
        self.bViewVisit.setObjectName("bViewVisit")
        self.verticalLayout.addWidget(self.bViewVisit)
        self.bDoctors = QtWidgets.QCommandLinkButton(self.boxMenu)
        self.bDoctors.setObjectName("bDoctors")
        self.verticalLayout.addWidget(self.bDoctors)
        self.bClients = QtWidgets.QCommandLinkButton(self.boxMenu)
        self.bClients.setObjectName("bClients")
        self.verticalLayout.addWidget(self.bClients)
        self.bReport = QtWidgets.QCommandLinkButton(self.boxMenu)
        self.bReport.setObjectName("bReport")
        self.verticalLayout.addWidget(self.bReport)
        self.horizontalLayout.addWidget(self.boxMenu)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)

        self.retranslateUi(StartMenu)
        QtCore.QMetaObject.connectSlotsByName(StartMenu)

    def retranslateUi(self, StartMenu):
        _translate = QtCore.QCoreApplication.translate
        StartMenu.setWindowTitle(_translate("StartMenu", "GroupBox"))
        StartMenu.setTitle(_translate("StartMenu", "Меню"))
        self.bNewVisit.setText(_translate("StartMenu", "Новове посещение"))
        self.bNewVisit.setDescription(_translate("StartMenu", "Быстрое создание нового посещения"))
        self.bViewVisit.setText(_translate("StartMenu", "Просмотр посещений"))
        self.bDoctors.setText(_translate("StartMenu", "Сотрудники"))
        self.bDoctors.setDescription(_translate("StartMenu", "Просмотр / Создание / Редактирование /Удаление \n"
" Сотрудников организации"))
        self.bClients.setText(_translate("StartMenu", "Клиенты"))
        self.bClients.setDescription(_translate("StartMenu", "Просмотр / Создание / Редактирование /Удаление \n"
" Клиентов"))
        self.bReport.setText(_translate("StartMenu", "Создать отчет"))
        self.bReport.setDescription(_translate("StartMenu", "Быстрое создание отчета статистики, выписка клиенту или врачу"))

