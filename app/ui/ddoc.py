# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/ddoc.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dDoc(object):
    def setupUi(self, dDoc):
        dDoc.setObjectName("dDoc")
        dDoc.resize(320, 352)
        dDoc.setMinimumSize(QtCore.QSize(320, 250))
        self.verticalLayout = QtWidgets.QVBoxLayout(dDoc)
        self.verticalLayout.setContentsMargins(9, -1, -1, 3)
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName("verticalLayout")
        self.ename = QtWidgets.QLineEdit(dDoc)
        self.ename.setMaxLength(16)
        self.ename.setObjectName("ename")
        self.verticalLayout.addWidget(self.ename)
        self.efamil = QtWidgets.QLineEdit(dDoc)
        self.efamil.setMaxLength(32)
        self.efamil.setObjectName("efamil")
        self.verticalLayout.addWidget(self.efamil)
        self.efather = QtWidgets.QLineEdit(dDoc)
        self.efather.setMaxLength(16)
        self.efather.setObjectName("efather")
        self.verticalLayout.addWidget(self.efather)
        self.ephone = QtWidgets.QLineEdit(dDoc)
        self.ephone.setInputMethodHints(QtCore.Qt.ImhPreferNumbers)
        self.ephone.setInputMask("")
        self.ephone.setFrame(True)
        self.ephone.setObjectName("ephone")
        self.verticalLayout.addWidget(self.ephone)
        self.eemail = QtWidgets.QLineEdit(dDoc)
        self.eemail.setInputMethodHints(QtCore.Qt.ImhUrlCharactersOnly)
        self.eemail.setMaxLength(96)
        self.eemail.setObjectName("eemail")
        self.verticalLayout.addWidget(self.eemail)
        self.eaddress = QtWidgets.QLineEdit(dDoc)
        self.eaddress.setObjectName("eaddress")
        self.verticalLayout.addWidget(self.eaddress)
        self.cbspec = QtWidgets.QLineEdit(dDoc)
        self.cbspec.setObjectName("cbspec")
        self.verticalLayout.addWidget(self.cbspec)
        self.btns = QtWidgets.QDialogButtonBox(dDoc)
        self.btns.setMaximumSize(QtCore.QSize(483, 27))
        self.btns.setOrientation(QtCore.Qt.Horizontal)
        self.btns.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.btns.setCenterButtons(False)
        self.btns.setObjectName("btns")
        self.verticalLayout.addWidget(self.btns)

        self.retranslateUi(dDoc)
        self.btns.accepted.connect(dDoc.accept)
        self.btns.rejected.connect(dDoc.reject)
        QtCore.QMetaObject.connectSlotsByName(dDoc)

    def retranslateUi(self, dDoc):
        _translate = QtCore.QCoreApplication.translate
        dDoc.setWindowTitle(_translate("dDoc", "Dialog"))
        self.ename.setPlaceholderText(_translate("dDoc", "Имя"))
        self.efamil.setPlaceholderText(_translate("dDoc", "Фамилия"))
        self.efather.setPlaceholderText(_translate("dDoc", "Отчество"))
        self.ephone.setPlaceholderText(_translate("dDoc", "Телефон"))
        self.eemail.setPlaceholderText(_translate("dDoc", "E-mail"))
        self.eaddress.setPlaceholderText(_translate("dDoc", "Адрес"))

