# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'auth.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OLogin(object):
    def setupUi(self, OLogin):
        OLogin.setObjectName("OLogin")
        OLogin.resize(220, 260)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(OLogin.sizePolicy().hasHeightForWidth())
        OLogin.setSizePolicy(sizePolicy)
        self.verticalLayout = QtWidgets.QVBoxLayout(OLogin)
        self.verticalLayout.setContentsMargins(3, 16, 3, 3)
        self.verticalLayout.setSpacing(12)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lhead = QtWidgets.QLabel(OLogin)
        self.lhead.setObjectName("lhead")
        self.verticalLayout.addWidget(self.lhead)
        self.elogin = QtWidgets.QLineEdit(OLogin)
        self.elogin.setObjectName("elogin")
        self.verticalLayout.addWidget(self.elogin)
        self.epwd = QtWidgets.QLineEdit(OLogin)
        self.epwd.setObjectName("epwd")
        self.verticalLayout.addWidget(self.epwd)
        self.sbtn = QtWidgets.QDialogButtonBox(OLogin)
        self.sbtn.setMinimumSize(QtCore.QSize(0, 42))
        self.sbtn.setOrientation(QtCore.Qt.Horizontal)
        self.sbtn.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.sbtn.setCenterButtons(True)
        self.sbtn.setObjectName("sbtn")
        self.verticalLayout.addWidget(self.sbtn)

        self.retranslateUi(OLogin)
        self.sbtn.accepted.connect(OLogin.accept)
        self.sbtn.rejected.connect(OLogin.reject)
        QtCore.QMetaObject.connectSlotsByName(OLogin)

    def retranslateUi(self, OLogin):
        _translate = QtCore.QCoreApplication.translate
        OLogin.setWindowTitle(_translate("OLogin", "Авторизация"))
        self.lhead.setText(_translate("OLogin", "<html><head/><body><p align=\"center\"><span style=\" font-size:14pt; font-weight:600;\">Авторизация</span></p></body></html>"))
        self.elogin.setPlaceholderText(_translate("OLogin", "Логин"))
        self.epwd.setPlaceholderText(_translate("OLogin", "Пароль"))

