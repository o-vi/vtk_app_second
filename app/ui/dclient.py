# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/dclient.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dClient(object):
    def setupUi(self, dClient):
        dClient.setObjectName("dClient")
        dClient.resize(280, 344)
        dClient.setMinimumSize(QtCore.QSize(280, 278))
        dClient.setMaximumSize(QtCore.QSize(384, 344))
        self.verticalLayout = QtWidgets.QVBoxLayout(dClient)
        self.verticalLayout.setObjectName("verticalLayout")
        self.ename = QtWidgets.QLineEdit(dClient)
        self.ename.setObjectName("ename")
        self.verticalLayout.addWidget(self.ename)
        self.efamil = QtWidgets.QLineEdit(dClient)
        self.efamil.setObjectName("efamil")
        self.verticalLayout.addWidget(self.efamil)
        self.efather = QtWidgets.QLineEdit(dClient)
        self.efather.setObjectName("efather")
        self.verticalLayout.addWidget(self.efather)
        self.ephone = QtWidgets.QLineEdit(dClient)
        self.ephone.setObjectName("ephone")
        self.verticalLayout.addWidget(self.ephone)
        self.eemail = QtWidgets.QLineEdit(dClient)
        self.eemail.setObjectName("eemail")
        self.verticalLayout.addWidget(self.eemail)
        self.eaddress = QtWidgets.QLineEdit(dClient)
        self.eaddress.setObjectName("eaddress")
        self.verticalLayout.addWidget(self.eaddress)
        self.btns = QtWidgets.QDialogButtonBox(dClient)
        self.btns.setOrientation(QtCore.Qt.Horizontal)
        self.btns.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.btns.setObjectName("btns")
        self.verticalLayout.addWidget(self.btns)

        self.retranslateUi(dClient)
        self.btns.accepted.connect(dClient.accept)
        self.btns.rejected.connect(dClient.reject)
        QtCore.QMetaObject.connectSlotsByName(dClient)

    def retranslateUi(self, dClient):
        _translate = QtCore.QCoreApplication.translate
        dClient.setWindowTitle(_translate("dClient", "Dialog"))
        self.ename.setPlaceholderText(_translate("dClient", "Имя"))
        self.efamil.setPlaceholderText(_translate("dClient", "Фамилия"))
        self.efather.setPlaceholderText(_translate("dClient", "Отчество"))
        self.ephone.setPlaceholderText(_translate("dClient", "Телефон"))
        self.eemail.setPlaceholderText(_translate("dClient", "E-mail"))
        self.eaddress.setPlaceholderText(_translate("dClient", "Адрес"))

