# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/editor.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_OEditor(object):
    def setupUi(self, OEditor, Frame):
        OEditor.setObjectName("OEditor")
        OEditor.resize(439, 512)
        self.verticalLayout = QtWidgets.QVBoxLayout(OEditor)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(OEditor)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout.addWidget(self.frame)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.bEditor = QtWidgets.QDialogButtonBox(OEditor)
        self.bEditor.setOrientation(QtCore.Qt.Horizontal)
        self.bEditor.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.bEditor.setObjectName("bEditor")
        self.verticalLayout.addWidget(self.bEditor)

        self.retranslateUi(OEditor)
        self.bEditor.accepted.connect(OEditor.accept)
        self.bEditor.rejected.connect(OEditor.reject)
        QtCore.QMetaObject.connectSlotsByName(OEditor)

    def retranslateUi(self, OEditor):
        _translate = QtCore.QCoreApplication.translate
        OEditor.setWindowTitle(_translate("OEditor", "Dialog"))

