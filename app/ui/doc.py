# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'doc.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Doc(object):
    def setupUi(self, Doc):
        Doc.setObjectName("Doc")
        Doc.setWindowModality(QtCore.Qt.WindowModal)
        Doc.resize(279, 227)
        Doc.setFrameShape(QtWidgets.QFrame.NoFrame)
        Doc.setFrameShadow(QtWidgets.QFrame.Plain)
        Doc.setLineWidth(0)
        self.verticalLayout = QtWidgets.QVBoxLayout(Doc)
        self.verticalLayout.setObjectName("verticalLayout")
        self.lname = QtWidgets.QLineEdit(Doc)
        self.lname.setObjectName("lname")
        self.verticalLayout.addWidget(self.lname)
        self.lfamil = QtWidgets.QLineEdit(Doc)
        self.lfamil.setObjectName("lfamil")
        self.verticalLayout.addWidget(self.lfamil)
        self.lfather = QtWidgets.QLineEdit(Doc)
        self.lfather.setObjectName("lfather")
        self.verticalLayout.addWidget(self.lfather)
        self.lphone = QtWidgets.QLineEdit(Doc)
        self.lphone.setObjectName("lphone")
        self.verticalLayout.addWidget(self.lphone)
        self.lemail = QtWidgets.QLineEdit(Doc)
        self.lemail.setObjectName("lemail")
        self.verticalLayout.addWidget(self.lemail)
        self.btns = QtWidgets.QDialogButtonBox(Doc)
        self.btns.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.btns.setCenterButtons(True)
        self.btns.setObjectName("btns")
        self.verticalLayout.addWidget(self.btns)

        self.retranslateUi(Doc)
        QtCore.QMetaObject.connectSlotsByName(Doc)

    def retranslateUi(self, Doc):
        _translate = QtCore.QCoreApplication.translate
        Doc.setWindowTitle(_translate("Doc", "Доктор"))
        self.lname.setPlaceholderText(_translate("Doc", "Имя"))
        self.lfamil.setPlaceholderText(_translate("Doc", "Фамилия"))
        self.lfather.setPlaceholderText(_translate("Doc", "Отчество"))
        self.lphone.setPlaceholderText(_translate("Doc", "Телефон"))
        self.lemail.setPlaceholderText(_translate("Doc", "E-mail"))

