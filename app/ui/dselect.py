# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/dselect.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dSelect(object):
    def setupUi(self, dSelect):
        dSelect.setObjectName("dSelect")
        dSelect.resize(323, 599)
        dSelect.setMinimumSize(QtCore.QSize(240, 320))
        dSelect.setMaximumSize(QtCore.QSize(323, 16777215))
        self.verticalLayout = QtWidgets.QVBoxLayout(dSelect)
        self.verticalLayout.setContentsMargins(1, 5, 1, 3)
        self.verticalLayout.setSpacing(2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.vlist = QtWidgets.QListView(dSelect)
        self.vlist.setObjectName("vlist")
        self.verticalLayout.addWidget(self.vlist)
        self.btns = QtWidgets.QDialogButtonBox(dSelect)
        self.btns.setOrientation(QtCore.Qt.Horizontal)
        self.btns.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.btns.setCenterButtons(True)
        self.btns.setObjectName("btns")
        self.verticalLayout.addWidget(self.btns)

        self.retranslateUi(dSelect)
        self.btns.accepted.connect(dSelect.accept)
        self.btns.rejected.connect(dSelect.reject)
        QtCore.QMetaObject.connectSlotsByName(dSelect)

    def retranslateUi(self, dSelect):
        _translate = QtCore.QCoreApplication.translate
        dSelect.setWindowTitle(_translate("dSelect", "Dialog"))

