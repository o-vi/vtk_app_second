# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'visit.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Visit(object):
    def setupUi(self, Visit):
        Visit.setObjectName("Visit")
        Visit.resize(604, 298)
        Visit.setFrameShape(QtWidgets.QFrame.StyledPanel)
        Visit.setFrameShadow(QtWidgets.QFrame.Raised)
        self.gridLayout = QtWidgets.QGridLayout(Visit)
        self.gridLayout.setObjectName("gridLayout")
        self.bclient = QtWidgets.QPushButton(Visit)
        self.bclient.setObjectName("bclient")
        self.gridLayout.addWidget(self.bclient, 3, 3, 1, 1)
        self.dtvisit = QtWidgets.QDateTimeEdit(Visit)
        self.dtvisit.setAcceptDrops(True)
        self.dtvisit.setObjectName("dtvisit")
        self.gridLayout.addWidget(self.dtvisit, 3, 4, 1, 1)
        self.edoc = QtWidgets.QLineEdit(Visit)
        self.edoc.setObjectName("edoc")
        self.gridLayout.addWidget(self.edoc, 1, 2, 1, 1)
        self.eclient = QtWidgets.QLineEdit(Visit)
        self.eclient.setObjectName("eclient")
        self.gridLayout.addWidget(self.eclient, 3, 2, 1, 1)
        self.lbdate = QtWidgets.QLabel(Visit)
        self.lbdate.setObjectName("lbdate")
        self.gridLayout.addWidget(self.lbdate, 1, 4, 1, 1, QtCore.Qt.AlignHCenter)
        self.lbclient = QtWidgets.QLabel(Visit)
        self.lbclient.setObjectName("lbclient")
        self.gridLayout.addWidget(self.lbclient, 3, 0, 1, 1)
        self.bdoc = QtWidgets.QPushButton(Visit)
        self.bdoc.setObjectName("bdoc")
        self.gridLayout.addWidget(self.bdoc, 1, 3, 1, 1)
        self.lbdoc = QtWidgets.QLabel(Visit)
        self.lbdoc.setObjectName("lbdoc")
        self.gridLayout.addWidget(self.lbdoc, 1, 0, 1, 1)
        self.btns = QtWidgets.QDialogButtonBox(Visit)
        self.btns.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.btns.setCenterButtons(True)
        self.btns.setObjectName("btns")
        self.gridLayout.addWidget(self.btns, 6, 3, 1, 2)
        self.ecost = QtWidgets.QLineEdit(Visit)
        self.ecost.setObjectName("ecost")
        self.gridLayout.addWidget(self.ecost, 6, 0, 1, 3)
        self.anamnesis = QtWidgets.QTextEdit(Visit)
        self.anamnesis.setEnabled(True)
        self.anamnesis.setObjectName("anamnesis")
        self.gridLayout.addWidget(self.anamnesis, 5, 0, 1, 5)

        self.retranslateUi(Visit)
        QtCore.QMetaObject.connectSlotsByName(Visit)

    def retranslateUi(self, Visit):
        _translate = QtCore.QCoreApplication.translate
        Visit.setWindowTitle(_translate("Visit", "Визит"))
        self.bclient.setText(_translate("Visit", "Выбрать"))
        self.lbdate.setText(_translate("Visit", "Дата визита"))
        self.lbclient.setText(_translate("Visit", "Клиент"))
        self.bdoc.setText(_translate("Visit", "Выбрать"))
        self.lbdoc.setText(_translate("Visit", "Доктор"))
        self.ecost.setPlaceholderText(_translate("Visit", "Стоимость посищения"))
        self.anamnesis.setHtml(_translate("Visit", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Заключение врача</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))

