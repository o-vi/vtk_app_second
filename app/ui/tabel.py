# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'table.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_TableView(object):
    def setupUi(self, TableView):
        TableView.setObjectName("TableView")
        TableView.resize(768, 480)
        TableView.setMinimumSize(QtCore.QSize(768, 480))
        self.gridLayout = QtWidgets.QGridLayout(TableView)
        self.gridLayout.setContentsMargins(0, -1, -1, -1)
        self.gridLayout.setObjectName("gridLayout")
        self.table = QtWidgets.QTableView(TableView)
        self.table.setObjectName("table")
        self.gridLayout.addWidget(self.table, 1, 0, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.bNew = QtWidgets.QPushButton(TableView)
        self.bNew.setObjectName("bNew")
        self.verticalLayout.addWidget(self.bNew)
        self.bEdit = QtWidgets.QPushButton(TableView)
        self.bEdit.setObjectName("bEdit")
        self.verticalLayout.addWidget(self.bEdit)
        self.bDel = QtWidgets.QPushButton(TableView)
        self.bDel.setObjectName("bDel")
        self.verticalLayout.addWidget(self.bDel)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.gridLayout.addLayout(self.verticalLayout, 0, 2, 2, 1)

        self.retranslateUi(TableView)
        QtCore.QMetaObject.connectSlotsByName(TableView)

    def retranslateUi(self, TableView):
        _translate = QtCore.QCoreApplication.translate
        TableView.setWindowTitle(_translate("TableView", "GroupBox"))
        TableView.setTitle(_translate("TableView", "Просмотр таблиц"))
        self.bNew.setText(_translate("TableView", "Создать"))
        self.bEdit.setText(_translate("TableView", "Изменить"))
        self.bDel.setText(_translate("TableView", "Удалить"))

