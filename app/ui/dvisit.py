# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui/dvisit.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dVisits(object):
    def setupUi(self, dVisits):
        dVisits.setObjectName("dVisits")
        dVisits.resize(765, 523)
        self.gridLayout = QtWidgets.QGridLayout(dVisits)
        self.gridLayout.setObjectName("gridLayout")
        self.eclient = QtWidgets.QLineEdit(dVisits)
        self.eclient.setObjectName("eclient")
        self.gridLayout.addWidget(self.eclient, 1, 1, 1, 1)
        self.bdoc = QtWidgets.QPushButton(dVisits)
        self.bdoc.setObjectName("bdoc")
        self.gridLayout.addWidget(self.bdoc, 0, 2, 1, 1)
        self.lbdoc = QtWidgets.QLabel(dVisits)
        self.lbdoc.setObjectName("lbdoc")
        self.gridLayout.addWidget(self.lbdoc, 0, 0, 1, 1)
        self.dtvisit = QtWidgets.QDateTimeEdit(dVisits)
        self.dtvisit.setAcceptDrops(True)
        self.dtvisit.setObjectName("dtvisit")
        self.gridLayout.addWidget(self.dtvisit, 1, 3, 1, 1)
        self.rvisit = QtWidgets.QRadioButton(dVisits)
        self.rvisit.setObjectName("rvisit")
        self.gridLayout.addWidget(self.rvisit, 5, 1, 1, 1)
        self.ecost = QtWidgets.QLineEdit(dVisits)
        self.ecost.setObjectName("ecost")
        self.gridLayout.addWidget(self.ecost, 5, 0, 1, 1)
        self.bclient = QtWidgets.QPushButton(dVisits)
        self.bclient.setObjectName("bclient")
        self.gridLayout.addWidget(self.bclient, 1, 2, 1, 1)
        self.anamnesis = QtWidgets.QTextEdit(dVisits)
        self.anamnesis.setEnabled(True)
        self.anamnesis.setObjectName("anamnesis")
        self.gridLayout.addWidget(self.anamnesis, 2, 0, 1, 4)
        self.lbdate = QtWidgets.QLabel(dVisits)
        self.lbdate.setObjectName("lbdate")
        self.gridLayout.addWidget(self.lbdate, 0, 3, 1, 1)
        self.lbclient = QtWidgets.QLabel(dVisits)
        self.lbclient.setObjectName("lbclient")
        self.gridLayout.addWidget(self.lbclient, 1, 0, 1, 1)
        self.btns = QtWidgets.QDialogButtonBox(dVisits)
        self.btns.setOrientation(QtCore.Qt.Horizontal)
        self.btns.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Save)
        self.btns.setObjectName("btns")
        self.gridLayout.addWidget(self.btns, 5, 2, 1, 2)
        self.edoc = QtWidgets.QLineEdit(dVisits)
        self.edoc.setObjectName("edoc")
        self.gridLayout.addWidget(self.edoc, 0, 1, 1, 1)

        self.retranslateUi(dVisits)
        self.btns.accepted.connect(dVisits.accept)
        self.btns.rejected.connect(dVisits.reject)
        QtCore.QMetaObject.connectSlotsByName(dVisits)

    def retranslateUi(self, dVisits):
        _translate = QtCore.QCoreApplication.translate
        dVisits.setWindowTitle(_translate("dVisits", "Dialog"))
        self.bdoc.setText(_translate("dVisits", "Выбрать"))
        self.lbdoc.setText(_translate("dVisits", "Доктор"))
        self.rvisit.setText(_translate("dVisits", "Факт посещения"))
        self.ecost.setPlaceholderText(_translate("dVisits", "Стоимость посищения"))
        self.bclient.setText(_translate("dVisits", "Выбрать"))
        self.anamnesis.setHtml(_translate("dVisits", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Ubuntu\'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Заключение врача</p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>"))
        self.lbdate.setText(_translate("dVisits", "Дата визита"))
        self.lbclient.setText(_translate("dVisits", "Клиент"))

