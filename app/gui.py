#!/usr/bin/python3.5
from ui import auth, main, startmenu, table, dvisit, ddoc, dclient, dselect, table
from PyQt5 import QtWidgets
from PyQt5 import QtSql, QtCore
import oDb
import time



def ifError(el, text, er=True):
	pass


class WidAuth(QtWidgets.QDialog, auth.Ui_OLogin):
	def __init__(self, parent=None):
		super(WidAuth, self).__init__(parent)
		self.setupUi(self)
		self.p = parent
		self.show()
		self.accept()

	def accept(self):
		# Change on text from form
		res = oDb.authorization_user('admin', '1')
		if res:
			self.lhead.setText('Авторизация прошла успешно')
			time.sleep(1)
			self.p.start()
			self.destroy()
		else:
			self.lhead.setText('Ошибка!\nповторите попытку')


# Основное окно
class WidMain(QtWidgets.QMainWindow, main.Ui_MainWindow):
	def __init__(self, parent=None):
		super(WidMain, self).__init__(parent)
		self.setupUi(self)
		WidAuth(self)

	def start(self):
		self.show()
		self.BoxCur = WidMenu(self.centralwidget, self)
		self.gridLayout.addWidget(self.BoxCur, 0, 0)
		self.BoxCur.show()

	def box_back(self):
		self.BoxCur.hide()
		self.BoxCur = self.BoxOld
		self.BoxCur.show()

	def box_change(self, box):
		self.BoxOld = self.BoxCur
		# self.toback.clicked.connect(self.fback)
		self.BoxOld.hide()
		self.BoxCur = None
		self.CurClass = box
		self.BoxCur = box(self.centralwidget, self)
		self.gridLayout.addWidget(self.BoxCur, 0, 0)
		self.BoxCur.show()

	def box_reset(self):
		self.BoxCur.hide()
		del self.BoxCur
		self.BoxCur = self.CurClass(self.centralwidget, self)
		self.gridLayout.addWidget(self.BoxCur, 0, 0)
		self.BoxCur.show()


# Менюшка на главной
class WidMenu(QtWidgets.QGroupBox, startmenu.Ui_StartMenu):
	def __init__(self, area, parent):
		super(WidMenu, self).__init__(parent)
		self.p = parent
		self.setupUi(self)
		self.btns_on()
		self.show()

	def btns_on(self):
		self.bNewVisit.clicked.connect(lambda: self.p.box_change(WidVisit))
		self.bViewVisit.clicked.connect(self.visits)
		self.bClients.clicked.connect(self.clients)
		self.bDoctors.clicked.connect(self.doctors)

	def clients(self):
		self.p.box_change(WidTable)
		self.p.BoxCur.start_for_pep('prog1.client')

	def doctors(self):
		self.p.box_change(WidTable)
		self.p.BoxCur.start_for_pep('prog1.doctor')

	def visits(self):
		self.p.box_change(WidTable)
		self.p.BoxCur.start_for_pep('prog1.view_visits')

# Просмотр таблицы
class WidTable(QtWidgets.QGroupBox, table.Ui_TableView):
	def __init__(self, area, parent):
		super(WidTable, self).__init__(parent)
		self.p = parent
		self.setupUi(self)

# Возможно эта идея будет жива
# Разменстить поля редактирования рядом с таблицей
	# def right_edit(self, widget):
	#   self.editor = widget(self, self)
	#   self.gridLayout.addWidget(self.editor, 0, 3, 2, 1)
	#   self.bEdit.hide()

	def init_button(self):
		self.bNew.clicked.connect(self.new_row)
		self.bDel.clicked.connect(self.delete_row)
		self.bSubmit.clicked.connect(self.submit_table)
		self.bUpdate.clicked.connect(self.revert_table)
		self.bEdit.clicked.connect(self.editor)
		self.table.doubleClicked.connect(self.editor)

	def start_for_pep(self, tb):
		self.tb = tb
		head = ['Номер', 'Имя', 'Фамилия', 'Отчество', 'Телефон', 'E-mail', 'Адрес']
		if str(tb) == 'prog1.doctor':
			print('i')
			head.append('Спец-сть')
			self.curTableClass = QtSql.QSqlRelationalTableModel
		elif str(tb) == oDb.db.shem + '.client':
			self.curTableClass = QtSql.QSqlTableModel
		elif str(tb) == oDb.db.shem + '.view_visits':
			head = ['Номер', 'Доктор', 'Клиент', 'Дата посищения',
				'Анамнезис', 'Стоимость', 'Явился']
			self.curTableClass = QtSql.QSqlTableModel

		self.tmodel = self.curTableClass(None, oDb.db)
		
		self.tmodel.setTable(tb)
		self.tmodel.setEditStrategy(self.curTableClass.OnFieldChange)

		if str(tb) == 'prog1.doctor':
			colSp = self.tmodel.fieldIndex('speciality_id')
			print(str(colSp)) 
			self.tmodel.setJoinMode(1)
			self.tmodel.setRelation(
				colSp,QtSql.QSqlRelation('prog1.list_specialist', 'id', 'name'))
			self.table.setItemDelegate(QtSql.QSqlRelationalDelegate(self.table))

		self.table.setModel(self.tmodel)

		for i, item in enumerate(head):
			self.tmodel.setHeaderData(i, QtCore.Qt.Horizontal, item)

		self.tmodel.select()
		self.table.hideColumn(0)
		self.init_button()

	def editor(self):
		tb = self.tb
		if tb == oDb.db.shem + '.doctor':
			self.curDlg = WidDoc(self.tmodel, self)
			self.curDlg.mpr.setCurrentModelIndex(
				self.table.currentIndex())
		elif tb == oDb.db.shem + '.client':
			self.curDlg = WidClinet(self.tmodel, self)
			self.curDlg.mpr.setCurrentModelIndex(
				self.table.currentIndex())
		elif tb == oDb.db.shem + '.view_visits':
			row = self.table.selectionModel().selectedRows()
			self.curDlg = WidVisit(self.tmodel.data(row[0]), self)


	def submit_table(self):
		self.tmodel.submitAll()
		self.tmodel.select()
		print(self.tmodel.lastError())

	def revert_table(self):
		self.tmodel.select()

	def delete_row(self):
		select = self.table.currentIndex().row()
		self.tmodel.removeRow(select)

	def new_row(self):
		self.tmodel.insertRow(self.tmodel.rowCount())
		self.table.setCurrentIndex(self.tmodel.index((self.tmodel.rowCount()-1), 0))
		self.editor()

class WidDoc(QtWidgets.QDialog, ddoc.Ui_dDoc):
	def __init__(self, model, parent):
		super(WidDoc, self).__init__(parent)
		self.p = parent
		self.setupUi(self)

		self.mpr = QtWidgets.QDataWidgetMapper()
		self.mpr.setModel(model)
		self.mpr.setItemDelegate(oDb.ComboBoxDelegate(self.mpr))
		self.mpr.addMapping(self.ename, 1)
		self.mpr.addMapping(self.efamil, 2)
		self.mpr.addMapping(self.efather, 3)
		self.mpr.addMapping(self.ephone, 4)
		self.mpr.addMapping(self.eemail, 5)
		self.mpr.addMapping(self.eaddress, 6)

		self.show()


class WidClinet(QtWidgets.QDialog, dclient.Ui_dClient):
	def __init__(self, model, parent):
		super(WidClinet, self).__init__(parent)
		self.p = parent
		self.m = model
		self.setupUi(self)

		self.mpr = QtWidgets.QDataWidgetMapper()
		self.mpr.setModel(model)
		self.mpr.addMapping(self.ename, 1)	
		self.mpr.addMapping(self.efamil, 2)
		self.mpr.addMapping(self.efather, 3)
		self.mpr.addMapping(self.ephone, 4)
		self.mpr.addMapping(self.eemail, 5)

		self.show()

class WidVisit(QtWidgets.QDialog, dvisit.Ui_dVisits):
	def __init__(self, item_id, parent):
		super(WidVisit, self).__init__(parent)
		self.p = parent
		self.i = item_id
		self.setupUi(self)

		self.m = QtSql.QSqlTableModel(None, oDb.db)
		self.m.setTable('prog1.visit')
		# p = QtSql.QSqlIndex('id','id')
		# self.m.setPrimaryKey(p)
		self.m.setFilter('id={}'.format(item_id))
		self.m.select()

		self.mpr = QtWidgets.QDataWidgetMapper()
		self.mpr.setModel(self.m)	

		self.mpr.addMapping(self.anamnesis, 4)
		self.mpr.addMapping(self.dtvisit, 3)
		self.mpr.addMapping(self.ecost, 5)
		self.mpr.addMapping(self.rvisit, 6)
		self.mpr.toFirst()

		self.show()
		QtCore.qDebug(self.m.lastError().text())
		
		self.bdoc.clicked.connect(lambda: self.select('doctor'))
		self.bclient.clicked.connect(lambda: self.select('client'))

		self.docComplete = MyComplete(self, 'doctor', 1, self.edoc)

	def accept(self):
		self.m.submitAll()
		self.p.revert_table()
		self.destroy()

	def select(self, tb):
		wid = WidSelect(tb, self)		


class MyComplete(QtWidgets.QCompleter):
	def __init__(self, parent, table, row, widget, *args):
		QtWidgets.QCompleter.__init__(self, parent)
		self.w = widget
		self.row = row
		self.q = QtSql.QSqlQueryModel(self)
		self.q.setQuery('SELECT id, concat_ws(\' \', name, familya, otchestvo) as fio FROM {}.{}'.format(oDb.db.shem, table))
		self.setModel(self.q)
		self.setCompletionColumn(1)
		widget.setCompleter(self)
		self.setCaseSensitivity(0)
		self.activated.connect(self.selected)

	def selected(self, i):
		print(str(self.currentRow()))
		
		cur_row = self.currentIndex().row()
		cur_row = self.q.createIndex(cur_row, 0)
		print(str(self.q.data(cur_row)))
		



class WidSelect(QtWidgets.QDialog, dselect.Ui_dSelect):
	def __init__(self, table, parent):
		super(WidSelect, self).__init__(parent)
		self.p = parent
		self.t = table
		self.setupUi(self)
		self.setModal(True)

		self.q = QtSql.QSqlQueryModel(None)
		self.q.setQuery('SELECT id, concat_ws(\' \', name, familya, otchestvo) as fio FROM {}.{}'.format(oDb.db.shem, table))
		
		self.vlist.setModel(self.q)
		self.vlist.setModelColumn(1)

		self.show()

		QtCore.qDebug(self.q.lastError().text())
	def accept(self):
		cur = self.vlist.currentIndex().row()
		cur = self.q.createIndex(cur, 0)
		item = self.q.data(cur)
		
		 
